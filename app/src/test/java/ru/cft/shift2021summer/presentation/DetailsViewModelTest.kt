package ru.cft.shift2021summer.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import ru.cft.shift2021summer.TestCoroutineRule
import ru.cft.shift2021summer.TestEntities.BEER_1
import ru.cft.shift2021summer.domain.useCases.favorites.AddFavoriteUseCase
import ru.cft.shift2021summer.presentation.details.DetailsViewModel

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class DetailsViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val addFavoriteUseCase: AddFavoriteUseCase = mockk {
        coEvery { addFavorite(BEER_1) } just runs
    }

    private val beerDomainMapper: BeerDomainMapper = BeerDomainMapper()

    private lateinit var viewModel: DetailsViewModel

    private val addObserver: Observer<BeerPresentation> = mockk()

    @Before
    fun prepare() {
        viewModel = DetailsViewModel(addFavoriteUseCase, beerDomainMapper)
        viewModel.addToFavoritesEvent.observeForever(addObserver)
    }

    @Test
    fun `add favorite use case EXPECT runs`() {
        testCoroutineRule.runBlockingTest {
            viewModel.addFavoritesUseCase(beerDomainMapper.mapFrom(BEER_1))
            coVerify { addFavoriteUseCase.addFavorite(BEER_1) }
        }
    }

//  Do not work! io.mockk.MockKException: no answer found for: Observer(#3).onChanged(...)

//    @Test
//    fun `add favorite EXPECT observe`() {
//        testCoroutineRule.runBlockingTest {
//            viewModel.addToFavorites(beerDomainMapper.mapFrom(BEER_1))
//            verify { addObserver.onChanged(beerDomainMapper.mapFrom(BEER_1)) }
//        }
//    }



}