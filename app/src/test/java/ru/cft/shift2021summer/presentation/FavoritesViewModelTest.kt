package ru.cft.shift2021summer.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.mockk.coEvery
import io.mockk.just
import io.mockk.mockk
import io.mockk.runs
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import ru.cft.shift2021summer.TestCoroutineRule
import ru.cft.shift2021summer.TestEntities.ALL_BEER
import ru.cft.shift2021summer.TestEntities.BEER_1
import ru.cft.shift2021summer.domain.useCases.favorites.DeleteAllFavoritesUseCase
import ru.cft.shift2021summer.domain.useCases.favorites.DeleteFavoriteUseCase
import ru.cft.shift2021summer.domain.useCases.favorites.GetAllFavoritesUseCase
import ru.cft.shift2021summer.presentation.favorites.FavoritesViewModel
import ru.cft.shift2021summer.utils.DataState

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class FavoritesViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val getAllFavoriteUseCase: GetAllFavoritesUseCase = mockk {
        coEvery { getAllFavorites() } returns ALL_BEER
    }

    private val deleteFavoriteUseCase: DeleteFavoriteUseCase = mockk {
        coEvery { deleteFavorite(BEER_1) } just runs
    }

    private val deleteAllFavoritesUseCase: DeleteAllFavoritesUseCase = mockk {
        coEvery { deleteAllFavorites() } just runs
    }

    private val beerDomainMapper: BeerDomainMapper = BeerDomainMapper()

    private lateinit var viewModel: FavoritesViewModel

    //Can not check all states because toList() collects latest state(loadFavorites begins in init block)
    @Test
    fun `get all favorites EXPECT success event`() {
        testCoroutineRule.runBlockingTest {
            viewModel = FavoritesViewModel(
                getAllFavoritesUseCase = getAllFavoriteUseCase,
                deleteAllFavoritesUseCase = deleteAllFavoritesUseCase,
                deleteFavoriteUseCase = deleteFavoriteUseCase,
                beerDomainMapper = beerDomainMapper
            )
            val result = mutableListOf<DataState<List<BeerPresentation>>>()
            val job = launch { viewModel.favoritesFlow.toList(result) }
            assertThat("LoadingState", result.first() is DataState.Success)
            job.cancel()
        }
    }
}