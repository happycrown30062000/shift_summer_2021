package ru.cft.shift2021summer.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import ru.cft.shift2021summer.TestCoroutineRule
import ru.cft.shift2021summer.TestEntities.BEER_1
import ru.cft.shift2021summer.domain.useCases.list.GetRandomBeerUseCase
import ru.cft.shift2021summer.presentation.random.RandomViewModel

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class RandomViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    private val getRandomBeerUseCase: GetRandomBeerUseCase = mockk {
        coEvery { getRandomBeer() } returns BEER_1
    }

    private val beerDomainMapper: BeerDomainMapper = BeerDomainMapper()

    private lateinit var viewModel: RandomViewModel

//    Тоже не работает, не понимаю почему
//
//    @Test
//    fun `load random beer EXPECT success event`() {
//        testCoroutineRule.runBlockingTest {
//            viewModel = RandomViewModel(getRandomBeerUseCase, beerDomainMapper)
//            val result = mutableListOf<DataState<BeerPresentation>>()
//            val job = launch { viewModel.beerPresentationFlow.toList(result) }
//            assertThat("Success state", result.first() is DataState.Success)
//            job.cancel()
//        }
//    }
}