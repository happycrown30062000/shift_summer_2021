package ru.cft.shift2021summer

import ru.cft.shift2021summer.data.BeerDto
import ru.cft.shift2021summer.domain.Beer

object TestEntities {

    const val QUERY = "Some_query"
    const val PAGE = 1
    const val PAGE_SIZE = 2

    val BEER_DTO_1 = BeerDto(
        alcohol = 5.0,
        description = "beer 1 description",
        firstBrewed = "01.01.2000",
        id = 1,
        imageUrl = null,
        name = "Beer_1",
        tagline = "Beer_1_is_good"
    )

    private val BEER_DTO_2 = BeerDto(
        alcohol = 4.0,
        description = "beer 2 description",
        firstBrewed = "02.01.2000",
        id = 2,
        imageUrl = null,
        name = "Beer_2",
        tagline = "Beer_2_is_good"
    )

    val BEER_1 = Beer(
        alcohol = 5.0,
        description = "beer 1 description",
        firstBrewed = "01.01.2000",
        id = 1,
        imageUrl = null,
        name = "Beer_1",
        tagline = "Beer_1_is_good"
    )

    private val BEER_2 = Beer(
        alcohol = 4.0,
        description = "beer 2 description",
        firstBrewed = "02.01.2000",
        id = 2,
        imageUrl = null,
        name = "Beer_2",
        tagline = "Beer_2_is_good"
    )

    val ALL_BEER_DTO = listOf(BEER_DTO_1, BEER_DTO_2)
    val ALL_BEER = listOf(BEER_1, BEER_2)

}