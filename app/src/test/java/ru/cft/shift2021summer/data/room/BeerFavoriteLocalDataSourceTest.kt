package ru.cft.shift2021summer.data.room

import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import ru.cft.shift2021summer.TestEntities.ALL_BEER_DTO
import ru.cft.shift2021summer.TestEntities.BEER_DTO_1

@RunWith(MockitoJUnitRunner::class)
class BeerFavoriteLocalDataSourceTest {

    private val beerDao: BeerDao = mockk {
        coEvery { getFavoriteBeer() } returns ALL_BEER_DTO
        coEvery { addFavoriteBeer(BEER_DTO_1) } just runs
        coEvery { deleteAllFavorites() } just runs
        coEvery { deleteFavoriteBeer(BEER_DTO_1) } just runs
    }

    private val beerFavoriteLocalDataSource: BeerFavoriteLocalDataSource = BeerFavoriteLocalDataSource(beerDao)

    @Test
    fun `get favorite beer EXPECT list of beer dto`() = runBlocking {
        assertEquals(ALL_BEER_DTO, beerFavoriteLocalDataSource.getAllFavorite())
    }

    @Test
    fun `add favorite beer EXPECT runs`() = runBlocking {
        beerFavoriteLocalDataSource.addFavorite(BEER_DTO_1)
        coVerify { beerDao.addFavoriteBeer(BEER_DTO_1) }
    }

    @Test
    fun `delete favorite beer EXPECT runs`() = runBlocking {
        beerFavoriteLocalDataSource.deleteFavorite(BEER_DTO_1)
        coVerify { beerDao.deleteFavoriteBeer(BEER_DTO_1) }
    }

    @Test
    fun `delete all favorite beer EXPECT runs`() = runBlocking {
        beerFavoriteLocalDataSource.deleteAll()
        coVerify { beerDao.deleteAllFavorites() }
    }

}