package ru.cft.shift2021summer.data

import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import ru.cft.shift2021summer.TestEntities.ALL_BEER_DTO
import ru.cft.shift2021summer.TestEntities.BEER_DTO_1
import ru.cft.shift2021summer.TestEntities.PAGE
import ru.cft.shift2021summer.TestEntities.PAGE_SIZE
import ru.cft.shift2021summer.TestEntities.QUERY
import ru.cft.shift2021summer.data.retrofit.BeerDataSource
import ru.cft.shift2021summer.data.room.BeerFavoriteDataSource

@RunWith(MockitoJUnitRunner::class)
class BeerRepositoryImplTest {

    private val beerRemoteDataSource: BeerDataSource = mockk {
        coEvery { getBeerQuery(QUERY, PAGE, PAGE_SIZE) } returns ALL_BEER_DTO
        coEvery { getRandomBeer() } returns listOf(BEER_DTO_1)
    }

    private val beerLocalDataSource: BeerFavoriteDataSource = mockk {
        coEvery { getAllFavorite() } returns ALL_BEER_DTO
        coEvery { addFavorite(BEER_DTO_1) } just runs
        coEvery { deleteFavorite(BEER_DTO_1) } just runs
        coEvery { deleteAll() } just runs
    }

    private val beerDtoMapper: BeerDtoMapper = BeerDtoMapper()

    private val repositoryImpl: BeerRepositoryImpl = BeerRepositoryImpl(
        beerDataSource = beerRemoteDataSource,
        beerDtoMapper = beerDtoMapper,
        beerFavoriteDataSource = beerLocalDataSource,
    )

    @Test
    fun `get beer query EXPECT list of beer`() = runBlocking {
        assertEquals(beerDtoMapper.mapDtoListToDomain(ALL_BEER_DTO), repositoryImpl.getBeerQuery(QUERY, PAGE, PAGE_SIZE))
    }

    @Test
    fun `get random beer EXPECT beer`() = runBlocking {
        assertEquals(beerDtoMapper.mapDtoListToDomain(listOf(BEER_DTO_1)), repositoryImpl.getRandomBeer())
    }

    @Test
    fun `get all favorites EXPECT list of beer`() = runBlocking {
        assertEquals(beerDtoMapper.mapDtoListToDomain(ALL_BEER_DTO), repositoryImpl.getAllFavorites())
    }

    @Test
    fun `add favorite EXPECT runs`() = runBlocking {
        repositoryImpl.addFavorite(beerDtoMapper.mapFrom(BEER_DTO_1))
        coVerify { beerLocalDataSource.addFavorite(BEER_DTO_1) }
    }

    @Test
    fun `delete favorite EXPECT runs`() = runBlocking {
        repositoryImpl.deleteFavorite(beerDtoMapper.mapFrom(BEER_DTO_1))
        coVerify { beerLocalDataSource.deleteFavorite(BEER_DTO_1) }
    }

    @Test
    fun `delete all favorites EXPECT runs`() = runBlocking {
        repositoryImpl.deleteAllFavorites()
        coVerify { beerLocalDataSource.deleteAll() }
    }

}