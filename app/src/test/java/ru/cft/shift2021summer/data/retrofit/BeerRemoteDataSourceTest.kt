package ru.cft.shift2021summer.data.retrofit

import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import ru.cft.shift2021summer.TestEntities.ALL_BEER_DTO
import ru.cft.shift2021summer.TestEntities.BEER_DTO_1
import ru.cft.shift2021summer.TestEntities.PAGE
import ru.cft.shift2021summer.TestEntities.PAGE_SIZE
import ru.cft.shift2021summer.TestEntities.QUERY

@RunWith(MockitoJUnitRunner::class)
class BeerRemoteDataSourceTest {

    private val api: BeerApi = mockk {
        coEvery { getBeerQuery(QUERY, PAGE, PAGE_SIZE) } returns ALL_BEER_DTO
        coEvery { getRandomBeer() } returns listOf(BEER_DTO_1)
    }

    private val dataSource: BeerRemoteDataSource = BeerRemoteDataSource(api)

    @Test
    fun `get beer query EXPECT list of beer dto`() = runBlocking {
        assertEquals(ALL_BEER_DTO, dataSource.getBeerQuery(QUERY, PAGE, PAGE_SIZE))
    }

    @Test
    fun `get random beer EXPECT one random beer dto`() = runBlocking {
        assertEquals(listOf(BEER_DTO_1), dataSource.getRandomBeer())
    }

}