package ru.cft.shift2021summer.domain.favorites

import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import ru.cft.shift2021summer.TestEntities.BEER_1
import ru.cft.shift2021summer.domain.BeerRepository
import ru.cft.shift2021summer.domain.useCases.favorites.AddFavoriteUseCase


@RunWith(MockitoJUnitRunner::class)
class AddFavoriteUseCaseTest {

    private val repository: BeerRepository = mockk {
        coEvery { addFavorite(BEER_1) } just runs
    }

    private val addFavoriteUseCase: AddFavoriteUseCase = AddFavoriteUseCase(repository)

    @Test
    fun `add favorite EXPECT runs`() = runBlocking {
        addFavoriteUseCase.addFavorite(BEER_1)
        coVerify { repository.addFavorite(BEER_1) }
    }
}