package ru.cft.shift2021summer.domain.list

import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import ru.cft.shift2021summer.TestEntities.BEER_1
import ru.cft.shift2021summer.domain.BeerRepository
import ru.cft.shift2021summer.domain.useCases.list.GetRandomBeerUseCase

@RunWith(MockitoJUnitRunner::class)
class GetRandomBeerUseCaseTest {
    private val repository: BeerRepository = mockk {
        coEvery { getRandomBeer() } returns listOf(BEER_1)
    }

    private val getRandomBeerUseCase: GetRandomBeerUseCase = GetRandomBeerUseCase(repository)

    @Test
    fun `get random beer EXPECT beer`() = runBlocking {
        Assert.assertEquals(BEER_1, getRandomBeerUseCase.getRandomBeer())
    }
}