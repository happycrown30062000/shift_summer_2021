package ru.cft.shift2021summer.domain.favorites

import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import ru.cft.shift2021summer.TestEntities.ALL_BEER
import ru.cft.shift2021summer.domain.BeerRepository
import ru.cft.shift2021summer.domain.useCases.favorites.GetAllFavoritesUseCase

@RunWith(MockitoJUnitRunner::class)
class GetAllFavoritesUseCaseTest {

    private val repository: BeerRepository = mockk {
        coEvery { getAllFavorites() } returns ALL_BEER
    }

    private val getAllFavoritesUseCase: GetAllFavoritesUseCase = GetAllFavoritesUseCase(repository)

    @Test
    fun `get all favorites EXPECT list of beer`() = runBlocking {
        assertEquals(ALL_BEER, getAllFavoritesUseCase.getAllFavorites())
    }
}