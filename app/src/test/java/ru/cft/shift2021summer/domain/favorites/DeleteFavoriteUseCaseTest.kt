package ru.cft.shift2021summer.domain.favorites

import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import ru.cft.shift2021summer.TestEntities.BEER_1
import ru.cft.shift2021summer.domain.BeerRepository
import ru.cft.shift2021summer.domain.useCases.favorites.DeleteFavoriteUseCase

@RunWith(MockitoJUnitRunner::class)
class DeleteFavoriteUseCaseTest {

    private val repository: BeerRepository = mockk {
        coEvery { deleteFavorite(BEER_1) } just runs
    }

    private val deleteFavoriteUseCase: DeleteFavoriteUseCase = DeleteFavoriteUseCase(repository)

    @Test
    fun `delete favorite EXPECT runs`() = runBlocking {
        deleteFavoriteUseCase.deleteFavorite(BEER_1)
        coVerify { repository.deleteFavorite(BEER_1) }
    }

}