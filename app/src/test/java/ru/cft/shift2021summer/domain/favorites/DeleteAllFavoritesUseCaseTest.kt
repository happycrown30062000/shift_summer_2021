package ru.cft.shift2021summer.domain.favorites

import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import ru.cft.shift2021summer.domain.BeerRepository
import ru.cft.shift2021summer.domain.useCases.favorites.DeleteAllFavoritesUseCase

@RunWith(MockitoJUnitRunner::class)
class DeleteAllFavoritesUseCaseTest {

    private val repository: BeerRepository = mockk {
        coEvery { deleteAllFavorites() } just runs
    }

    private val deleteAllFavoriteUseCase: DeleteAllFavoritesUseCase = DeleteAllFavoritesUseCase(repository)

    @Test
    fun `delete all favorites EXPECT runs`() = runBlocking {
        deleteAllFavoriteUseCase.deleteAllFavorites()
        coVerify { repository.deleteAllFavorites() }
    }
}