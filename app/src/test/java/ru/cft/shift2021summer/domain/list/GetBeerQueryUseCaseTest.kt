package ru.cft.shift2021summer.domain.list

import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import ru.cft.shift2021summer.TestEntities.ALL_BEER
import ru.cft.shift2021summer.TestEntities.PAGE
import ru.cft.shift2021summer.TestEntities.PAGE_SIZE
import ru.cft.shift2021summer.TestEntities.QUERY
import ru.cft.shift2021summer.domain.BeerRepository
import ru.cft.shift2021summer.domain.useCases.list.GetBeerQueryUseCase

@RunWith(MockitoJUnitRunner::class)
class GetBeerQueryUseCaseTest {

    private val repository: BeerRepository = mockk {
        coEvery { getBeerQuery(QUERY, PAGE, PAGE_SIZE) } returns ALL_BEER
    }

    private val getBeerQueryUseCase: GetBeerQueryUseCase = GetBeerQueryUseCase(repository)

    @Test
    fun `get beer query EXPECT list of beer`() = runBlocking {
        assertEquals(ALL_BEER, getBeerQueryUseCase.getBeerQuery(QUERY, PAGE, PAGE_SIZE))
    }

}