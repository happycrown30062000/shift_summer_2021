package ru.cft.shift2021summer.presentation.tests

import androidx.test.core.app.ActivityScenario
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import org.junit.Test
import ru.cft.shift2021summer.R
import ru.cft.shift2021summer.presentation.MainActivity
import ru.cft.shift2021summer.presentation.screens.BeerListScreen

class BeerListScreenTest: TestCase() {

    @Test
    fun testBeerListScree() {
        run {
            step("Check we in list screen") {
                ActivityScenario.launch(MainActivity::class.java)
                BeerListScreen {
                    bottomNav {
                        isVisible()
                        hasSelectedItem(R.id.listFragment)
                    }

                    searchBar {
                        isVisible()
                        hasHint(R.string.search_hint)
                    }
                }
            }
        }
    }
}