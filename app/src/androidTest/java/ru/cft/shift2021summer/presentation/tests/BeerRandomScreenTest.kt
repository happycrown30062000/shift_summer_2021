package ru.cft.shift2021summer.presentation.tests

import androidx.test.core.app.ActivityScenario
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import org.junit.Test
import ru.cft.shift2021summer.R
import ru.cft.shift2021summer.presentation.MainActivity
import ru.cft.shift2021summer.presentation.screens.BeerRandomScreen

class BeerRandomScreenTest: TestCase() {

    @Test
    fun testRandomScreen() {
        run {
            step("Check we in random screen") {
                ActivityScenario.launch(MainActivity::class.java)
                BeerRandomScreen {
                    bottomNav {
                        setSelectedItem(R.id.randomFragment)
                    }
                    headerText {
                        hasText(R.string.random_beer_text)
                    }
                    detailsButton {
                        isVisible()
                    }
                    refreshButton {
                        isVisible()
                    }
                    shareButton {
                        isVisible()
                    }
                }
            }
        }
    }
}