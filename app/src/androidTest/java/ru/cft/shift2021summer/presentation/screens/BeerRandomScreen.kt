package ru.cft.shift2021summer.presentation.screens

import com.kaspersky.kaspresso.screens.KScreen
import io.github.kakaocup.kakao.bottomnav.KBottomNavigationView
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView
import ru.cft.shift2021summer.R
import ru.cft.shift2021summer.presentation.MainActivity

object BeerRandomScreen : KScreen<BeerRandomScreen>() {
    override val layoutId: Int = R.layout.activity_main
    override val viewClass: Class<*> = MainActivity::class.java

    val bottomNav = KBottomNavigationView { withId(R.id.bottom_nav) }
    val headerText = KTextView { withId(R.id.header_text) }
    val detailsButton = KButton { withId(R.id.details_button) }
    val shareButton = KButton { withId(R.id.share_button) }
    val refreshButton = KButton { withId(R.id.refresh_button) }
}