package ru.cft.shift2021summer.presentation.screens

import com.kaspersky.kaspresso.screens.KScreen
import io.github.kakaocup.kakao.bottomnav.KBottomNavigationView
import io.github.kakaocup.kakao.text.KButton
import ru.cft.shift2021summer.R
import ru.cft.shift2021summer.presentation.MainActivity

object BeerFavoritesListScreen: KScreen<BeerFavoritesListScreen>() {

    override val layoutId: Int = R.layout.activity_main
    override val viewClass: Class<*> = MainActivity::class.java

    val clearButton = KButton { withId(R.id.clear_cache_button) }
    val bottomNav = KBottomNavigationView {withId(R.id.bottom_nav)}
}