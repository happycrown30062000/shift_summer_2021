package ru.cft.shift2021summer.presentation.screens

import com.kaspersky.kaspresso.screens.KScreen
import io.github.kakaocup.kakao.bottomnav.KBottomNavigationView
import io.github.kakaocup.kakao.edit.KTextInputLayout
import ru.cft.shift2021summer.R
import ru.cft.shift2021summer.presentation.MainActivity

object BeerListScreen: KScreen<BeerListScreen>() {
    override val layoutId: Int = R.layout.activity_main
    override val viewClass: Class<*> = MainActivity::class.java

    val bottomNav = KBottomNavigationView {withId(R.id.bottom_nav)}
    val searchBar = KTextInputLayout { withId(R.id.text_input_layout)}
}