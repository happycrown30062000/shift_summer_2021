package ru.cft.shift2021summer.presentation.tests

import androidx.test.core.app.ActivityScenario
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import org.junit.Test
import ru.cft.shift2021summer.R
import ru.cft.shift2021summer.presentation.MainActivity
import ru.cft.shift2021summer.presentation.screens.BeerFavoritesListScreen

class BeerFavoritesListScreenTest: TestCase() {

    @Test
    fun testFavoriteScreen() {
        run {
            step("Check we in favorites screen") {
                ActivityScenario.launch(MainActivity::class.java)
                BeerFavoritesListScreen {
                    bottomNav {
                        setSelectedItem(R.id.favoritesFragment)
                    }
                    clearButton {
                        isVisible()
                        hasText(R.string.clear_caches_button_text)
                    }
                }
            }
        }
    }
}