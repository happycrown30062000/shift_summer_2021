package ru.cft.shift2021summer.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.cft.shift2021summer.data.*
import ru.cft.shift2021summer.data.retrofit.BeerApi
import ru.cft.shift2021summer.data.retrofit.BeerRemoteDataSource
import ru.cft.shift2021summer.data.BeerRepositoryImpl
import ru.cft.shift2021summer.data.retrofit.BeerDataSource
import ru.cft.shift2021summer.data.room.BeerDao
import ru.cft.shift2021summer.data.room.BeerFavoriteDataSource
import ru.cft.shift2021summer.data.room.BeerFavoriteLocalDataSource
import ru.cft.shift2021summer.presentation.BeerDomainMapper
import ru.cft.shift2021summer.domain.BeerRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideBeerDtoMapper(): BeerDtoMapper {
        return BeerDtoMapper()
    }

    @Singleton
    @Provides
    fun provideBeerDomainMapper(): BeerDomainMapper {
        return BeerDomainMapper()
    }

    @Singleton
    @Provides
    fun provideBeerRemoteDataSource(beerApi: BeerApi): BeerDataSource {
        return BeerRemoteDataSource(beerApi)
    }

    @Singleton
    @Provides
    fun provideBeerFavoriteLocalDataSource(beerDao: BeerDao): BeerFavoriteDataSource {
        return BeerFavoriteLocalDataSource(beerDao)
    }

    @Singleton
    @Provides
    fun provideBeerRepositoryImpl(
        beerRemoteDataSource: BeerDataSource,
        beerLocalDataSource: BeerFavoriteDataSource,
        beerDtoMapper: BeerDtoMapper
    ): BeerRepository {
        return BeerRepositoryImpl(
            beerRemoteDataSource,
            beerLocalDataSource,
            beerDtoMapper
        )
    }

}