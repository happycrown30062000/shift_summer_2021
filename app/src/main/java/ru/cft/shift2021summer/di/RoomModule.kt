package ru.cft.shift2021summer.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ru.cft.shift2021summer.data.room.BeerDao
import ru.cft.shift2021summer.data.room.BeerDatabase
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    @Singleton
    @Provides
    fun provideBeerDatabase(@ApplicationContext context: Context): BeerDatabase {
        return Room
            .databaseBuilder(context, BeerDatabase::class.java, BeerDatabase.DATABASE_NAME)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideBeerDao(database: BeerDatabase): BeerDao {
        return database.getBeerDao()
    }

}