package ru.cft.shift2021summer.domain.useCases.list

import ru.cft.shift2021summer.domain.Beer
import ru.cft.shift2021summer.domain.BeerRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetRandomBeerUseCase @Inject constructor(
    private val beerRepository: BeerRepository
) {

    suspend fun getRandomBeer(): Beer {
        return beerRepository.getRandomBeer().first()
    }
}


