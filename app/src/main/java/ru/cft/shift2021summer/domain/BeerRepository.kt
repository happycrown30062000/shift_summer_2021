package ru.cft.shift2021summer.domain

interface BeerRepository {

    //retrofit + paging
    suspend fun getBeerQuery(query: String, page: Int, perPage: Int): List<Beer>
    suspend fun getRandomBeer(): List<Beer>
    //room
    suspend fun getAllFavorites(): List<Beer>
    suspend fun addFavorite(beer: Beer)
    suspend fun deleteFavorite(beer: Beer)
    suspend fun deleteAllFavorites()


}