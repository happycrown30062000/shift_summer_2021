package ru.cft.shift2021summer.domain.useCases.favorites

import ru.cft.shift2021summer.domain.Beer
import ru.cft.shift2021summer.domain.BeerRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DeleteFavoriteUseCase @Inject constructor(
    private val beerRepository: BeerRepository
) {
    suspend fun deleteFavorite(beerPresentation: Beer) {
        beerRepository.deleteFavorite(beerPresentation)
    }

}