package ru.cft.shift2021summer.domain

data class Beer(
    val alcohol: Double,
    val description: String,
    val firstBrewed: String,
    val id: Int,
    val imageUrl: String?,
    val name: String,
    val tagline: String
)
