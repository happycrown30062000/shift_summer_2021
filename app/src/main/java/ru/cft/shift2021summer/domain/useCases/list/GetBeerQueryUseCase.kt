package ru.cft.shift2021summer.domain.useCases.list

import ru.cft.shift2021summer.domain.Beer
import ru.cft.shift2021summer.domain.BeerRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetBeerQueryUseCase @Inject constructor(
    private val beerRepository: BeerRepository
    ) {

    suspend fun getBeerQuery(query: String, page: Int, perPage: Int): List<Beer> {
        return beerRepository.getBeerQuery(query, page, perPage)
    }

}