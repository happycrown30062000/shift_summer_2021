package ru.cft.shift2021summer.utils

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import ru.cft.shift2021summer.R

fun ImageView.loadSimpleImage(context: Context, url: String?) {
    Glide.with(context).load(url).placeholder(R.drawable.ic_barrel).into(this)
}
