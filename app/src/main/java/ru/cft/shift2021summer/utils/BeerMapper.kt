package ru.cft.shift2021summer.utils

interface BeerMapper<From, To> {

    fun mapFrom(from: From): To
    fun mapTo(to: To): From

}