package ru.cft.shift2021summer.presentation.list

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import ru.cft.shift2021summer.R
import ru.cft.shift2021summer.databinding.FragmentListBinding
import ru.cft.shift2021summer.presentation.BeerPresentation

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class ListFragment : Fragment(R.layout.fragment_list) {

    private val binding: FragmentListBinding by viewBinding(FragmentListBinding::bind)
    private val viewModel: ListViewModel by viewModels()

    private val adapter: ListItemPagingAdapter by lazy {
        ListItemPagingAdapter(onClick = { currentBeer ->
            viewModel.openDetailsScreen(currentBeer)
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            adapter.addLoadStateListener { state ->
                beerList.isVisible = state.refresh != LoadState.Loading
                listProgress.isVisible = state.refresh == LoadState.Loading
            }
            beerList.adapter = adapter.withLoadStateHeaderAndFooter(
                header = LoaderBeerStateAdapter(),
                footer = LoaderBeerStateAdapter()
            )
            beerList.layoutManager = LinearLayoutManager(requireContext())
            searchBar.doAfterTextChanged { text ->
                viewModel.setQuery(text?.toString() ?: "")
            }
        }

        setObservers()
    }

    private fun setObservers() {
        viewModel.openDetailsEvent.observe(viewLifecycleOwner) { currentBeer ->
            navigateToBeerDetails(currentBeer)
        }

        lifecycleScope.launch {
            viewModel.beerPresentationList
                .flowWithLifecycle(lifecycle, Lifecycle.State.CREATED)
                .collectLatest { adapter.submitData(it) }
        }
    }

    private fun navigateToBeerDetails(currentBeerPresentation: BeerPresentation) {
        val action = ListFragmentDirections.actionListFragmentToDetailsFragment(currentBeerPresentation)
        findNavController().navigate(action)
    }

}