package ru.cft.shift2021summer.presentation

import ru.cft.shift2021summer.domain.Beer
import ru.cft.shift2021summer.utils.BeerMapper

class BeerDomainMapper: BeerMapper<Beer, BeerPresentation> {

    override fun mapFrom(from: Beer): BeerPresentation {
        return BeerPresentation(
            alcohol = from.alcohol,
            description = from.description,
            firstBrewed = from.firstBrewed,
            id = from.id,
            imageUrl = from.imageUrl,
            name = from.name,
            tagline = from.tagline
        )
    }

    override fun mapTo(to: BeerPresentation): Beer {
        return Beer(
            alcohol = to.alcohol,
            description = to.description,
            firstBrewed = to.firstBrewed,
            id = to.id,
            imageUrl = to.imageUrl,
            name = to.name,
            tagline = to.tagline
        )
    }

    fun mapDomainListToPresent(list: List<Beer>): List<BeerPresentation> {
        return list.map {
            mapFrom(it)
        }
    }

    fun mapPresentListToDomain(list: List<BeerPresentation>): List<Beer> {
        return list.map {
            mapTo(it)
        }
    }
}