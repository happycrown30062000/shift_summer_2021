package ru.cft.shift2021summer.presentation.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import ru.cft.shift2021summer.domain.useCases.favorites.AddFavoriteUseCase
import ru.cft.shift2021summer.presentation.BeerDomainMapper
import ru.cft.shift2021summer.presentation.BeerPresentation
import ru.cft.shift2021summer.utils.SingleLiveEvent
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel @Inject constructor(
    private val addFavoriteUseCase: AddFavoriteUseCase,
    private val beerDomainMapper: BeerDomainMapper
): ViewModel() {

    val addToFavoritesEvent = SingleLiveEvent<BeerPresentation>()

    fun addFavoritesUseCase(beerPresentation: BeerPresentation) {
        viewModelScope.launch {
            addFavoriteUseCase.addFavorite(beerDomainMapper.mapTo(beerPresentation))
        }
    }

    fun addToFavorites(beerPresentation: BeerPresentation) {
        addToFavoritesEvent.value = beerPresentation
    }

}