package ru.cft.shift2021summer.presentation.details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import ru.cft.shift2021summer.R
import ru.cft.shift2021summer.databinding.FragmentDetailsBinding
import ru.cft.shift2021summer.presentation.BeerPresentation
import ru.cft.shift2021summer.utils.loadSimpleImage

@AndroidEntryPoint
class DetailsFragment : Fragment(R.layout.fragment_details) {

    private val binding: FragmentDetailsBinding by viewBinding(FragmentDetailsBinding::bind)
    val viewModel: DetailsViewModel by viewModels()
    private val args by navArgs<DetailsFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindBeer(args.beer)
        setObservers()
    }

    private fun bindBeer(currentBeerPresentation: BeerPresentation) {
        val context = requireContext()
        with(binding) {
            beerDrawable.loadSimpleImage(context, currentBeerPresentation.imageUrl)
            beerAbv.text = context.getString(R.string.alcohol_format, currentBeerPresentation.alcohol.toString())
            beerDesc.text = currentBeerPresentation.description
            beerFirstBrewed.text = context.getString(R.string.first_brewed_format, currentBeerPresentation.firstBrewed)
            beerName.text = currentBeerPresentation.name
            beerTagline.text = currentBeerPresentation.tagline
            addToFavoriteButton.setOnClickListener {
                viewModel.addToFavorites(currentBeerPresentation)
            }
        }
    }

    private fun setObservers() {
        viewModel.addToFavoritesEvent.observe(viewLifecycleOwner){
            viewModel.addFavoritesUseCase(it)
            Snackbar.make(requireView(), R.string.add_successfully, Snackbar.LENGTH_SHORT).show()
        }
    }



}