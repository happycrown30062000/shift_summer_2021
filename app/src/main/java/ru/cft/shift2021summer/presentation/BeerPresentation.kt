package ru.cft.shift2021summer.presentation

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BeerPresentation(
    val alcohol: Double,
    val description: String,
    val firstBrewed: String,
    val id: Int,
    val imageUrl: String?,
    val name: String,
    val tagline: String
): Parcelable
