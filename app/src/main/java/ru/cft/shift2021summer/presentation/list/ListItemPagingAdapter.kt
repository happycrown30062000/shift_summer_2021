package ru.cft.shift2021summer.presentation.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import ru.cft.shift2021summer.R
import ru.cft.shift2021summer.databinding.ListItemBinding
import ru.cft.shift2021summer.presentation.BeerPresentation
import ru.cft.shift2021summer.utils.loadSimpleImage

class ListItemPagingAdapter(private val onClick: (BeerPresentation) -> Unit) :
    PagingDataAdapter<BeerPresentation, BeerViewHolder>(BeerDiffItemCallback) {

    override fun onBindViewHolder(holder: BeerViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerViewHolder {
        return BeerViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false),
            onClick
        )
    }
}

class BeerViewHolder(itemView: View, private val onClick: (BeerPresentation) -> Unit) :
    RecyclerView.ViewHolder(itemView) {
    private val binding: ListItemBinding by viewBinding(ListItemBinding::bind)

    fun bind(beerPresentation: BeerPresentation?) {
        with(binding) {
            beerPresentation?.let { currentBeer ->
                root.setOnClickListener {
                    onClick(currentBeer)
                }
                beerDrawable.loadSimpleImage(itemView.context, currentBeer.imageUrl)
            }
            beerName.text = beerPresentation?.name ?: "Can not find this beer"
            beerTagline.text = beerPresentation?.tagline
        }
    }
}

private object BeerDiffItemCallback : DiffUtil.ItemCallback<BeerPresentation>() {

    override fun areItemsTheSame(oldItem: BeerPresentation, newItem: BeerPresentation): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: BeerPresentation, newItem: BeerPresentation): Boolean {
        return oldItem.name == newItem.name
                && oldItem.tagline == newItem.tagline
                && oldItem.imageUrl == newItem.imageUrl
    }
}