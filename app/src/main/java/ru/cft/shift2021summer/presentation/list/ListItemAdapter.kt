package ru.cft.shift2021summer.presentation.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import ru.cft.shift2021summer.R
import ru.cft.shift2021summer.databinding.ListItemBinding
import ru.cft.shift2021summer.presentation.BeerPresentation
import ru.cft.shift2021summer.utils.loadSimpleImage

class ListItemAdapter(private val onClick: (BeerPresentation) -> Unit) : RecyclerView.Adapter<BeerItemViewHolder>() {

    var beerList = mutableListOf<BeerPresentation>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerItemViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return BeerItemViewHolder(itemView, onClick)
    }

    override fun onBindViewHolder(holder: BeerItemViewHolder, position: Int) {
        val selectedBeer = beerList[position]
        holder.bind(selectedBeer)
    }

    override fun getItemCount(): Int {
        return beerList.count()
    }

    fun removeItem(position: Int) {
        beerList.removeAt(position)
        notifyItemRemoved(position)
    }

}

class BeerItemViewHolder(itemView: View, private val onClick: (BeerPresentation) -> Unit) : RecyclerView.ViewHolder(itemView) {
    private val binding: ListItemBinding by viewBinding(ListItemBinding::bind)

    fun bind(beerPresentation: BeerPresentation) {
        with(binding) {
            beerDrawable.loadSimpleImage(itemView.context, beerPresentation.imageUrl)
            beerName.text = beerPresentation.name
            beerTagline.text = beerPresentation.tagline
            root.setOnClickListener {
                onClick(beerPresentation)
            }
        }
    }
}

class SwipeLeftCallback(
    private val adapter: ListItemAdapter,
    private val onRemove: (BeerPresentation) -> Unit
): ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val beer = adapter.beerList[viewHolder.absoluteAdapterPosition]
        adapter.removeItem(viewHolder.absoluteAdapterPosition)
        onRemove(beer)
    }
}

