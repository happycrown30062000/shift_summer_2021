package ru.cft.shift2021summer.presentation.random

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import ru.cft.shift2021summer.domain.useCases.list.GetRandomBeerUseCase
import ru.cft.shift2021summer.presentation.BeerDomainMapper
import ru.cft.shift2021summer.presentation.BeerPresentation
import ru.cft.shift2021summer.utils.DataState
import ru.cft.shift2021summer.utils.SingleLiveEvent
import javax.inject.Inject

@HiltViewModel
class RandomViewModel @Inject constructor(
    private val getRandomBeerUseCase: GetRandomBeerUseCase,
    private val beerDomainMapper: BeerDomainMapper
) : ViewModel() {

    init {
        loadRandomBeer()
    }

    private val _beerPresentationFlow: MutableStateFlow<DataState<BeerPresentation>> = MutableStateFlow(DataState.Empty)
    val beerPresentationFlow: StateFlow<DataState<BeerPresentation>> = _beerPresentationFlow.asStateFlow()

    val buttonEvent = SingleLiveEvent<Event>()

    fun openDetailsScreen(beerPresentation: BeerPresentation) {
        buttonEvent.value = Event.NavigateToDetails(beerPresentation)
    }

    fun shareBeer(beerPresentation: BeerPresentation) {
        buttonEvent.value = Event.ShareBeer(beerPresentation)
    }

    fun refreshBeer() {
        buttonEvent.value = Event.RefreshBeer
    }

    fun loadRandomBeer() {
        viewModelScope.launch {
            try {
                _beerPresentationFlow.value = DataState.Loading
                val beer = getRandomBeerUseCase.getRandomBeer()
                _beerPresentationFlow.value = DataState.Success(beerDomainMapper.mapFrom(beer))
            } catch (ex: Exception) {
                _beerPresentationFlow.value = DataState.Error(ex)
            }
        }

    }

    sealed class Event {
        data class NavigateToDetails(val beer: BeerPresentation) : Event()
        object RefreshBeer : Event()
        data class ShareBeer(val beer: BeerPresentation) : Event()
    }

}