package ru.cft.shift2021summer.presentation.random

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import ru.cft.shift2021summer.R
import ru.cft.shift2021summer.databinding.FragmentRandomBinding
import ru.cft.shift2021summer.presentation.BeerPresentation
import ru.cft.shift2021summer.utils.DataState
import ru.cft.shift2021summer.utils.loadSimpleImage

@AndroidEntryPoint
class RandomFragment : Fragment(R.layout.fragment_random) {

    private val binding: FragmentRandomBinding by viewBinding(FragmentRandomBinding::bind)
    private val viewModel: RandomViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            refreshButton.setOnClickListener {
                viewModel.refreshBeer()
            }
        }
        setObservers()
    }

    private fun setObservers() {
        lifecycleScope.launch {
            viewModel.beerPresentationFlow
                .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .collect { dataState ->
                    determineState(dataState)
                }
        }

        viewModel.buttonEvent.observe(viewLifecycleOwner) { buttonEvent ->
            when (buttonEvent) {
                is RandomViewModel.Event.RefreshBeer -> {
                    viewModel.loadRandomBeer()
                }
                is RandomViewModel.Event.NavigateToDetails -> {
                    navigateToBeerDetails(buttonEvent.beer)
                }
                is RandomViewModel.Event.ShareBeer -> {
                    shareBeer(buttonEvent.beer)
                }
            }
        }
    }

    private fun bindBeer(currentBeer: BeerPresentation) {
        with(binding) {
            beerDrawable.loadSimpleImage(requireContext(), currentBeer.imageUrl)
            beerName.text = currentBeer.name
            beerTagline.text = currentBeer.tagline
        }
    }

    private fun shareBeer(currentBeer: BeerPresentation) {
        val context = requireContext()
        val shareIntent = Intent(Intent.ACTION_SEND).apply {
            type = "plain/text"
            putExtra(
                Intent.EXTRA_TEXT,
                context.getString(R.string.share_beer, currentBeer.name)
            )
        }
        startActivity(Intent.createChooser(shareIntent, context.getString(R.string.share_via)))
    }

    private fun navigateToBeerDetails(currentBeerPresentation: BeerPresentation) {
        val action =
            RandomFragmentDirections.actionRandomFragmentToDetailsFragment(currentBeerPresentation)
        findNavController().navigate(action)
    }

    private fun determineState(dataState: DataState<BeerPresentation>) {
        when (dataState) {
            is DataState.Success -> {
                val currentBeer = dataState.data
                with(binding) {
                    detailsButton.isEnabled = true
                    content.isVisible = true
                    detailsButton.setOnClickListener {
                        viewModel.openDetailsScreen(currentBeer)
                    }
                    shareButton.setOnClickListener {
                        viewModel.shareBeer(currentBeer)
                    }
                }
                bindBeer(currentBeer)
            }
            is DataState.Loading -> {
                with(binding) {
                    beerName.text = context?.getString(R.string.loading_beer)
                    detailsButton.isEnabled = false
                    content.isVisible = true
                }
            }
            is DataState.Error -> {
                with(binding) {
                    beerName.text = context?.getString(R.string.loading_beer_error)
                    detailsButton.isEnabled = false
                    content.isVisible = true
                }
            }
            is DataState.Empty -> {
                with(binding) {
                    content.isVisible = false
                    detailsButton.isEnabled = false
                }
            }
        }
    }
}