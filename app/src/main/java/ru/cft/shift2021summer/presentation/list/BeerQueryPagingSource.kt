package ru.cft.shift2021summer.presentation.list

import androidx.paging.PagingSource
import androidx.paging.PagingState
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import retrofit2.HttpException
import ru.cft.shift2021summer.data.retrofit.BeerApi
import ru.cft.shift2021summer.domain.useCases.list.GetBeerQueryUseCase
import ru.cft.shift2021summer.presentation.BeerDomainMapper
import ru.cft.shift2021summer.presentation.BeerPresentation

class BeerQueryPagingSource @AssistedInject constructor(
    private val getBeerQueryUseCase: GetBeerQueryUseCase,
    private val beerDomainMapper: BeerDomainMapper,
    @Assisted("query") private val query: String
) : PagingSource<Int, BeerPresentation>() {
    override fun getRefreshKey(state: PagingState<Int, BeerPresentation>): Int? {
        val anchorPosition = state.anchorPosition ?: return null
        val anchorPage = state.closestPageToPosition(anchorPosition) ?: return null
        return anchorPage.prevKey?.plus(1) ?: anchorPage.nextKey?.minus(1)
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, BeerPresentation> {
        return try {
            val pageNumber = params.key ?: 1
            val pageSize = params.loadSize.coerceAtMost(BeerApi.MAX_PAGE_SIZE)
            val beerList = getBeerQueryUseCase.getBeerQuery(query, pageNumber, pageSize)
            val beerPresentationList = beerDomainMapper.mapDomainListToPresent(beerList)
            val nextPageNumber = if (beerList.isEmpty()) null else pageNumber + 1
            val prevPageNumber = if (pageNumber > 1) pageNumber - 1 else null
            LoadResult.Page(beerPresentationList, prevPageNumber, nextPageNumber)
        } catch (e: HttpException) {
            LoadResult.Error(e)
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(@Assisted("query") query: String): BeerQueryPagingSource
    }

}