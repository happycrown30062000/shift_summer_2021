package ru.cft.shift2021summer.presentation.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import ru.cft.shift2021summer.presentation.BeerPresentation
import ru.cft.shift2021summer.utils.SingleLiveEvent
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class ListViewModel @Inject constructor(
    private val beerQueryPagingSourceFactory: BeerQueryPagingSource.Factory
): ViewModel() {

    val openDetailsEvent = SingleLiveEvent<BeerPresentation>()
    fun openDetailsScreen(beerPresentation: BeerPresentation) {
        openDetailsEvent.value = beerPresentation
    }

    private val _queryFlow = MutableStateFlow("")
    fun setQuery(query: String) {
        _queryFlow.tryEmit(query)
    }

    private var newPagingSource: PagingSource<Int, BeerPresentation>? = null
    private fun newPager(query: String): Pager<Int, BeerPresentation> {
        return Pager(PagingConfig(5, enablePlaceholders = false)) {
            newPagingSource?.invalidate()
            beerQueryPagingSourceFactory.create(query).also {
                newPagingSource = it
            }
        }
    }

    val beerPresentationList: StateFlow<PagingData<BeerPresentation>> = _queryFlow
        .map { query -> newPager(query) }
        .flatMapLatest { pager -> pager.flow }
        .cachedIn(viewModelScope)
        .stateIn(viewModelScope, SharingStarted.Lazily, PagingData.empty())

}