package ru.cft.shift2021summer.presentation.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import ru.cft.shift2021summer.databinding.ErrorItemBinding
import ru.cft.shift2021summer.databinding.ProgressItemBinding

class LoaderBeerStateAdapter : LoadStateAdapter<LoaderBeerStateAdapter.ItemViewHolder>() {

    companion object {
        private const val ERROR = 1
        private const val PROGRESS = 0
    }

    abstract class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(loadState: LoadState)
    }

    override fun getStateViewType(loadState: LoadState): Int {
        return when (loadState) {
            is LoadState.Loading -> PROGRESS
            is LoadState.Error -> ERROR
            is LoadState.NotLoading -> error("Not support error state")
        }
    }

    override fun onBindViewHolder(holder: ItemViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): ItemViewHolder {
        return when (loadState) {
            is LoadState.Loading -> ProgressViewHolder(LayoutInflater.from(parent.context), parent)
            is LoadState.Error -> ErrorViewHolder(LayoutInflater.from(parent.context), parent)
            is LoadState.NotLoading -> error("Not support error state")
        }
    }

    class ProgressViewHolder constructor(
        binding: ProgressItemBinding
    ) : ItemViewHolder(binding.root) {

        override fun bind(loadState: LoadState) {
            //do nothing
        }

        companion object {
            operator fun invoke(
                layoutInflater: LayoutInflater,
                parent: ViewGroup
            ): ProgressViewHolder {
                return ProgressViewHolder(
                    ProgressItemBinding.inflate(
                        layoutInflater,
                        parent,
                        false
                    )
                )
            }
        }
    }

    class ErrorViewHolder(private val binding: ErrorItemBinding): ItemViewHolder(binding.root) {
        override fun bind(loadState: LoadState) {
            require(loadState is LoadState.Error)
            binding.errorText.text = loadState.error.localizedMessage
        }

        companion object {
            operator fun invoke(
                layoutInflater: LayoutInflater,
                parent: ViewGroup
            ): ErrorViewHolder {
                return ErrorViewHolder(
                    ErrorItemBinding.inflate(
                        layoutInflater,
                        parent,
                        false
                    )
                )
            }
        }

    }
}