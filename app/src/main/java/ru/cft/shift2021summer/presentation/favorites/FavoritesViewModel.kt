package ru.cft.shift2021summer.presentation.favorites

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import ru.cft.shift2021summer.domain.useCases.favorites.DeleteAllFavoritesUseCase
import ru.cft.shift2021summer.domain.useCases.favorites.DeleteFavoriteUseCase
import ru.cft.shift2021summer.domain.useCases.favorites.GetAllFavoritesUseCase
import ru.cft.shift2021summer.presentation.BeerDomainMapper
import ru.cft.shift2021summer.presentation.BeerPresentation
import ru.cft.shift2021summer.utils.DataState
import ru.cft.shift2021summer.utils.SingleLiveEvent
import javax.inject.Inject

@HiltViewModel
class FavoritesViewModel @Inject constructor(
    private val getAllFavoritesUseCase: GetAllFavoritesUseCase,
    private val deleteFavoriteUseCase: DeleteFavoriteUseCase,
    private val deleteAllFavoritesUseCase: DeleteAllFavoritesUseCase,
    private val beerDomainMapper: BeerDomainMapper
): ViewModel() {

    private val _favoritesFlow: MutableStateFlow<DataState<List<BeerPresentation>>> = MutableStateFlow(DataState.Empty)
    val favoritesFlow: StateFlow<DataState<List<BeerPresentation>>> = _favoritesFlow

    val screenEvent = SingleLiveEvent<Event>()

    fun openDetailsScreen(beerPresentation: BeerPresentation) {
        screenEvent.value = Event.NavigateToDetails(beerPresentation)
    }

    fun clearCaches() {
        screenEvent.value = Event.ClearCaches
    }

    init {
        loadFavorites()
    }

    private fun loadFavorites() {
        viewModelScope.launch {
            try {
                _favoritesFlow.value = DataState.Loading
                val list = getAllFavoritesUseCase.getAllFavorites()
                _favoritesFlow.value = DataState.Success(beerDomainMapper.mapDomainListToPresent(list))
            } catch (ex: Exception) {
                _favoritesFlow.value = DataState.Error(ex)
            }
        }

    }

    fun deleteFavorite(beerPresentation: BeerPresentation) {
        viewModelScope.launch {
            deleteFavoriteUseCase.deleteFavorite(beerDomainMapper.mapTo(beerPresentation))
        }
    }

    fun deleteAllFavorites() {
        viewModelScope.launch {
            deleteAllFavoritesUseCase.deleteAllFavorites()
        }
    }

    sealed class Event {
        object ClearCaches: Event()
        data class NavigateToDetails(val beerPresentation: BeerPresentation): Event()
    }

}