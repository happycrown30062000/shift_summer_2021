package ru.cft.shift2021summer.presentation.favorites

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import ru.cft.shift2021summer.R
import ru.cft.shift2021summer.databinding.FragmentFavoritesBinding
import ru.cft.shift2021summer.presentation.BeerPresentation
import ru.cft.shift2021summer.presentation.list.ListItemAdapter
import ru.cft.shift2021summer.presentation.list.SwipeLeftCallback
import ru.cft.shift2021summer.utils.DataState

@AndroidEntryPoint
class FavoritesFragment : Fragment(R.layout.fragment_favorites) {

    private val binding: FragmentFavoritesBinding by viewBinding(FragmentFavoritesBinding::bind)
    val viewModel: FavoritesViewModel by viewModels()

    private val adapter by lazy {
        ListItemAdapter(onClick = {
            viewModel.openDetailsScreen(it)
        })
    }

    private val swipeCallback by lazy {
        SwipeLeftCallback(adapter, onRemove = {
            viewModel.deleteFavorite(it)
            Snackbar.make(requireView(), R.string.remove_successfully, Snackbar.LENGTH_SHORT).show()
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding){
            favoritesList.adapter = adapter
            favoritesList.layoutManager = LinearLayoutManager(requireContext())
            val touchHelper = ItemTouchHelper(swipeCallback)
            touchHelper.attachToRecyclerView(favoritesList)
            clearCacheButton.setOnClickListener {
                viewModel.clearCaches()
            }
        }
        setObservers()
    }

    private fun setObservers() {
        lifecycleScope.launch {
            viewModel.favoritesFlow
                .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .collect { dataState ->
                    determineState(dataState)
                }
        }

        viewModel.screenEvent.observe(viewLifecycleOwner) { event ->
            when (event) {
                is FavoritesViewModel.Event.NavigateToDetails -> {
                    navigateToBeerDetails(event.beerPresentation)
                }
                is FavoritesViewModel.Event.ClearCaches -> {
                    viewModel.deleteAllFavorites()
                    adapter.beerList = mutableListOf()
                    adapter.notifyDataSetChanged()
                    Snackbar.make(requireView(), R.string.caches_cleared, Snackbar.LENGTH_SHORT).show()
                }
            }

        }
    }

    private fun determineState(dataState: DataState<List<BeerPresentation>>) {
        when (dataState) {
            is DataState.Success -> {
                with(binding) {
                    favoritesProgressBar.isVisible = false
                    favoritesList.isVisible = true
                }
                adapter.beerList = dataState.data.toMutableList()
            }
            is DataState.Loading -> {
                with(binding) {
                    favoritesProgressBar.isVisible = true
                    favoritesList.isVisible = false
                }
            }
            is DataState.Empty -> {
                with(binding) {
                    favoritesProgressBar.isVisible = false
                    favoritesList.isVisible = false
                }
            }
            is DataState.Error -> {
                Snackbar.make(requireView(), R.string.loading_beer_error, Snackbar.LENGTH_LONG).show()
            }
        }
    }

    private fun navigateToBeerDetails(currentBeerPresentation: BeerPresentation) {
        val action = FavoritesFragmentDirections.actionFavoritesFragmentToDetailsFragment(currentBeerPresentation)
        findNavController().navigate(action)
    }
}