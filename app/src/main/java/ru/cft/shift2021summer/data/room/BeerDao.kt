package ru.cft.shift2021summer.data.room

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import ru.cft.shift2021summer.data.BeerDto

@Dao
abstract class BeerDao {

    @Query("SELECT * FROM BeerDto")
    abstract suspend fun getFavoriteBeer(): List<BeerDto>

    @Insert(onConflict = REPLACE)
    abstract suspend fun addFavoriteBeer(beerDto: BeerDto)

    @Delete
    abstract suspend fun deleteFavoriteBeer(beerDto: BeerDto)

    @Query("DELETE FROM BeerDto")
    abstract suspend fun deleteAllFavorites()

}