package ru.cft.shift2021summer.data.room

import ru.cft.shift2021summer.data.BeerDto

class BeerFavoriteLocalDataSource(private val beerDao: BeerDao): BeerFavoriteDataSource {
    override suspend fun getAllFavorite(): List<BeerDto> {
        return beerDao.getFavoriteBeer()
    }

    override suspend fun addFavorite(beerDto: BeerDto) {
        beerDao.addFavoriteBeer(beerDto)
    }

    override suspend fun deleteFavorite(beerDto: BeerDto) {
        beerDao.deleteFavoriteBeer(beerDto)
    }

    override suspend fun deleteAll() {
        beerDao.deleteAllFavorites()
    }
}