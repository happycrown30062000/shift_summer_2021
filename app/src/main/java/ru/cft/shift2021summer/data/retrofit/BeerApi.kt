package ru.cft.shift2021summer.data.retrofit

import androidx.annotation.IntRange
import retrofit2.http.GET
import retrofit2.http.Query
import ru.cft.shift2021summer.data.BeerDto

interface BeerApi {
    companion object {
        const val DEFAULT_PAGE_SIZE = 20
        const val MAX_PAGE_SIZE = 20
    }

    @GET("beers")
    suspend fun getBeerQuery(
        @Query("beer_name") beerName: String? = null,
        @Query("page") @IntRange(from = 1) page: Int = 1,
        @Query("per_page") @IntRange(from = 1, to = MAX_PAGE_SIZE.toLong()) per_page: Int = DEFAULT_PAGE_SIZE
    ): List<BeerDto>

    @GET("beers")
    suspend fun getBeerQuery(
        @Query("page") @IntRange(from = 1) page: Int = 1,
        @Query("per_page") @IntRange(from = 1, to = MAX_PAGE_SIZE.toLong()) per_page: Int = DEFAULT_PAGE_SIZE
    ): List<BeerDto>

    @GET("beers/random")
    suspend fun getRandomBeer(): List<BeerDto>
}