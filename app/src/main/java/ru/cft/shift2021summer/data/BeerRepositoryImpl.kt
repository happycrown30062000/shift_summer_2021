package ru.cft.shift2021summer.data

import ru.cft.shift2021summer.data.retrofit.BeerDataSource
import ru.cft.shift2021summer.data.room.BeerFavoriteDataSource
import ru.cft.shift2021summer.domain.Beer
import ru.cft.shift2021summer.domain.BeerRepository

class BeerRepositoryImpl(
    private val beerDataSource: BeerDataSource,
    private val beerFavoriteDataSource: BeerFavoriteDataSource,
    private val beerDtoMapper: BeerDtoMapper
): BeerRepository {

    //retrofit + paging
    override suspend fun getBeerQuery(query: String, page: Int, perPage: Int): List<Beer> {
        return beerDtoMapper.mapDtoListToDomain(beerDataSource.getBeerQuery(query, page, perPage))
    }

    override suspend fun getRandomBeer(): List<Beer> {
        return beerDtoMapper.mapDtoListToDomain(beerDataSource.getRandomBeer())
    }

    //room
    override suspend fun getAllFavorites(): List<Beer> {
        return beerDtoMapper.mapDtoListToDomain(beerFavoriteDataSource.getAllFavorite())
    }

    override suspend fun addFavorite(beer: Beer) {
        beerFavoriteDataSource.addFavorite(beerDtoMapper.mapTo(beer))
    }

    override suspend fun deleteFavorite(beer: Beer) {
        beerFavoriteDataSource.deleteFavorite(beerDtoMapper.mapTo(beer))
    }

    override suspend fun deleteAllFavorites() {
        beerFavoriteDataSource.deleteAll()
    }
}