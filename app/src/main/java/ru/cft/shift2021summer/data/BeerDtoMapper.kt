package ru.cft.shift2021summer.data

import ru.cft.shift2021summer.domain.Beer
import ru.cft.shift2021summer.utils.BeerMapper

class BeerDtoMapper: BeerMapper<BeerDto, Beer> {

    override fun mapFrom(from: BeerDto): Beer {
        return Beer(
            alcohol = from.alcohol,
            description = from.description,
            firstBrewed = from.firstBrewed,
            id = from.id,
            imageUrl = from.imageUrl,
            name = from.name,
            tagline = from.tagline
        )
    }

    override fun mapTo(to: Beer): BeerDto {
        return BeerDto(
            alcohol = to.alcohol,
            description = to.description,
            firstBrewed = to.firstBrewed,
            id = to.id,
            imageUrl = to.imageUrl,
            name = to.name,
            tagline = to.tagline
        )
    }

    fun mapDtoListToDomain(entityList: List<BeerDto>): List<Beer> {
        return entityList.map {
            mapFrom(it)
        }
    }

    fun mapDomainListToDto(list: List<Beer>): List<BeerDto> {
        return list.map {
            mapTo(it)
        }
    }
}