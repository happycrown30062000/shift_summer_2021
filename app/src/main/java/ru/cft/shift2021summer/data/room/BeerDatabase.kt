package ru.cft.shift2021summer.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.cft.shift2021summer.data.BeerDto

@Database(entities = [BeerDto::class], exportSchema = false, version = 1)
abstract class BeerDatabase: RoomDatabase() {
    companion object {
        const val DATABASE_NAME = "Beer database"
    }

    abstract fun getBeerDao(): BeerDao
}