package ru.cft.shift2021summer.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "BeerDto")
data class BeerDto(
    @SerializedName("abv")
    val alcohol: Double,
    val description: String,
    @SerializedName("first_brewed")
    val firstBrewed: String,
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    @SerializedName("image_url")
    val imageUrl: String?,
    val name: String,
    val tagline: String
)