package ru.cft.shift2021summer.data.retrofit

import ru.cft.shift2021summer.data.BeerDto

class BeerRemoteDataSource(
    private val beerApi: BeerApi
    ): BeerDataSource {

    override suspend fun getBeerQuery(query: String, page: Int, pageSize: Int): List<BeerDto> {
        return if (query.isBlank()) {
            beerApi.getBeerQuery(page, pageSize)
        } else {
            beerApi.getBeerQuery(query, page, pageSize)
        }
    }

    override suspend fun getRandomBeer(): List<BeerDto> {
        return beerApi.getRandomBeer()
    }
}