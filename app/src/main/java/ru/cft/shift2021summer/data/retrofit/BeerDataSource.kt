package ru.cft.shift2021summer.data.retrofit

import ru.cft.shift2021summer.data.BeerDto

interface BeerDataSource {
    suspend fun getBeerQuery(query: String, page: Int, pageSize: Int): List<BeerDto>
    suspend fun getRandomBeer(): List<BeerDto>
}