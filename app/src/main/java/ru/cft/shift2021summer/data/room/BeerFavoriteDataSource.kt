package ru.cft.shift2021summer.data.room

import ru.cft.shift2021summer.data.BeerDto

interface BeerFavoriteDataSource {

    suspend fun getAllFavorite(): List<BeerDto>
    suspend fun addFavorite(beerDto: BeerDto)
    suspend fun deleteFavorite(beerDto: BeerDto)
    suspend fun deleteAll()

}